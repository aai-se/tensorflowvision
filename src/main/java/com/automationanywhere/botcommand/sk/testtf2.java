

package com.automationanywhere.botcommand.sk;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import org.tensorflow.DataType;
import org.tensorflow.Graph;
import org.tensorflow.Output;
import org.tensorflow.SavedModelBundle;
import org.tensorflow.Session;
import org.tensorflow.Session.Runner;
import org.tensorflow.Tensor;
import org.tensorflow.TensorFlow;
import org.tensorflow.Tensors;
import org.tensorflow.types.UInt8;

import tsutils.Box;
import tsutils.DetectedObj;

/** Sample use of the TensorFlow Java API to label images using a pre-trained model. */
public class testtf2 {
  private static void printUsage(PrintStream s) {
    final String url =
        "https://storage.googleapis.com/download.tensorflow.org/models/inception5h.zip";
    s.println(
        "Java program that uses a pre-trained Inception model (http://arxiv.org/abs/1512.00567)");
    s.println("to label JPEG images.");
    s.println("TensorFlow version: " + TensorFlow.version());
    s.println();
    s.println("Usage: label_image <model dir> <image file>");
    s.println();
    s.println("Where:");
    s.println("<model dir> is a directory containing the unzipped contents of the inception model");
    s.println("            (from " + url + ")");
    s.println("<image file> is the path to a JPEG image file");
  }

  
  /* CLASSIFICTAION   */
  
  public static void main(String[] args) throws IOException {

    String modelDir = "C:/Users/Stefan Karsten/Google Drive/Colab Notebooks/BERT/export/1581691428";
    String imageFile ="C:\\temp\\sample12.jpg";

    
    SavedModelBundle model = SavedModelBundle.load(modelDir,"serve");


    
    Image image = ImageIO.read(new File(imageFile));
    BufferedImage buffered = (BufferedImage) image;
    String text = "Angela Merkel is the new prime minister";
    
	String[] inputs = new String[] { "Angela Merkel is the new prime minister" };

	byte[][] stringMatrix = new byte[1][];
	for (int i = 0; i < 1; ++i) {
		stringMatrix[i] = String.format(inputs[i]).getBytes("UTF-8");
	}
	


	 // Java Strings will need to be encoded into a byte-sequence.
	 String mystring = "foo";
	 Tensor<String> s = Tensor.create(mystring.getBytes("UTF-8"), String.class);

	 // Valid: Matrix of String tensors.
	 // Each element might have a different length.
	 byte[][][] matrix = new byte[1][1][];
	 matrix[0][0] = "this".getBytes("UTF-8");
	 Tensor<String> m = Tensor.create(matrix, String.class);

	  
	  List<Tensor<?>> outputs = model.session().runner().feed("input_example_tensor", m).fetch("loss/LogSoftmax").run();
    
//	Tensor<String> input = Tensors.create(stringMatrix);
   
 /*   Tensor input = Tensor.create(text.getBytes("UTF-8"));
        List<Tensor<?>> outputs = model
		        .session()
		        .runner()
		        .feed("input_example_tensor", input)
		        .fetch("loss/LogSoftmax")
		        .run();
*/
    Tensor<Float> output0 =   (Tensor<Float>)outputs.get(0);
    
    System.out.println("Dim"+output0.numDimensions());
    System.out.println("Size"+output0.shape().length);
    
  //  List<String> labels = readAllLinesOrExit(Paths.get(modelDir, "labels.txt"));
   
    Tensor<Float> scoresT = outputs.get(0).expect(Float.class);
   //         Tensor<Float> classesT = outputs.get(1).expect(Float.class);
   //         Tensor<Float> boxesT = outputs.get(2).expect(Float.class) ;
            
           // All these tensors have:
           // - 1 as the first dimension
           // - maxObjects as the second dimension
           // While boxesT will have 4 as the third dimension (2 sets of (x, y) coordinates).
           // This can be verified by looking at scoresT.shape() etc.
           int maxObjects = (int) scoresT.shape()[1];
           float[] scores = scoresT.copyTo(new float[3][maxObjects])[1];

           for (int i = 0; i < scores.length; ++i) {
               if (scores[i] < 0.1) {
                   continue;
               }
               float score = scores[i];
               
     //          System.out.println("Class "+labels.get(i));
               
               System.out.println("Score "+score);

     //          System.out.println("box"+box[0]+","+box[1]+","+box[2]+","+box[3]);

           }
       

           
        
           
 //   BufferedImage imagenew = drawDetectedObjects(buffered, result);
    
    
    File outputfile = new File("C:\\\\temp\\\\persons_box.jpg");
 //   ImageIO.write(imagenew, "jpg", outputfile);
    
    
 /*   byte[] graphDef = readAllBytesOrExit(Paths.get(modelDir, "saved_model.pb"));
    List<String> labels =
        readAllLinesOrExit(Paths.get(modelDir, "labels.txt"));
    byte[] imageBytes = readAllBytesOrExit(Paths.get(imageFile));

    try (Tensor<Float> image = constructAndExecuteGraphToNormalizeImage(imageBytes)) {
      float[] labelProbabilities = executeInceptionGraph(graphDef, image);
      int bestLabelIdx = maxIndex(labelProbabilities);
      System.out.println(
          String.format("BEST MATCH: %s (%.2f%% likely)",
              labels.get(bestLabelIdx),
              labelProbabilities[bestLabelIdx] * 100f));
    }
    
    */
  }
  
  
 
  
  public static BufferedImage drawDetectedObjects(BufferedImage img, List<DetectedObj> objList) {


      BufferedImage result = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
      Graphics g = result.getGraphics();
      g.drawImage(img, 0, 0, null);

      g.setColor(Color.yellow);

      for(DetectedObj obj : objList){
          Box box = obj.getBox();
          int x = (int)(box.getLeft() * img.getWidth());
          int y = (int)(box.getTop() * img.getHeight());
          g.drawString(obj.getLabel(), x, y);
          int width = (int)(box.getWidth() * img.getWidth());
          int height = (int)(box.getHeight() * img.getHeight());
          g.drawRect(x, y, width, height);
      }

      return result;
  }

  
}

